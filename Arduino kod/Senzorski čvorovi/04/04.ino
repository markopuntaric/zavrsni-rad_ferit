#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <BH1750.h>
#include <MQUnifiedsensor.h>
#include <SPI.h>  
#include "RF24.h" 



#define SEALEVELPRESSURE_HPA (1013.25)
const int soundPin = A1;


RF24 myRadio (7, 8); 
Adafruit_BME280 bme;
BH1750 lightMeter;


byte addresses[][6] = {"Node0","Node1","Node2","Node3","Node4"}; 

struct dataTransmitted{
  char a[4];
  float temp;
  float pres;
  float alt;
  float hum;
  float luxe;
  int sound;
  int ppm;
};

dataTransmitted data={ "S04",0,0,0,0,0,0,0};
int count=0;
bool ok;

void setup()  
{
  delay(1000);
  myRadio.begin();  
  myRadio.setChannel(108);  
  myRadio.setPALevel(RF24_PA_MAX);  
  myRadio.setDataRate(RF24_2MBPS); 

  myRadio.openWritingPipe(addresses[1]); 

  bme.begin(0x76);
  Wire.begin();
  lightMeter.begin();
  delay(1000);
  
}


void loop()  
{
  
  CollectData();

  do{
    count++;
  ok=myRadio.write( &data, sizeof(data) ); 
  delay(100);
  }while(count<=15 && ok==false);

  count=0;
  delay(5000);

}

void CollectData(){
  data.temp=bme.readTemperature();
  data.pres=bme.readPressure() / 100.0F;
  data.alt=bme.readAltitude(SEALEVELPRESSURE_HPA);
  data.hum=bme.readHumidity();
  data.luxe=lightMeter.readLightLevel();
  data.sound=analogRead (soundPin);
}

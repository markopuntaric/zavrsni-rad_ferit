//UKLJUČITI POTREBNE BIBLIOTEKE
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <BH1750.h>
#include "MQ135.h"
#include <SPI.h>  
#include "RF24.h" 

//DEFINIRATI KONSTANTE I PINOVE
#define SEALEVELPRESSURE_HPA (1013.25)
const int soundPin = A1;
const int MQ135PIN=0;

RF24 myRadio (7, 8); // "myRadio" RF24 KORIŠTEN ZA OSTVARIVANJE KOMUNIKACIJE
Adafruit_BME280 bme; // BME280
BH1750 lightMeter;   // BH1750
MQ135 gasSensor = MQ135(MQ135PIN); //MQ135


byte addresses[][6] = {"Node0","Node1","Node2","Node3","Node4"}; // ADRESA SVAKE CIJEVI ZA SLUŠANJE

struct dataTransmitted{
  char a[4];
  float temp;
  float pres;
  float alt;
  float hum;
  float luxe;
  int sound;
  int ppm;
};

dataTransmitted data={ "S01",0,0,0,0,0,0,0}; 
int count=0;
bool ok;

void setup()   //SETUP FUNKCIJA
{
  myRadio.begin();  // POKRENI NRF24L01 RADIO
  myRadio.setChannel(108);  // 108 JE KANAL IZNAD VEĆINE WIFI KANALA
  myRadio.setPALevel(RF24_PA_MAX);  // MAKSIMALNA SNAGA
  myRadio.setDataRate(RF24_2MBPS); // MAKSIMALNA BRZINA PRIJENOSA PODATAKA

  myRadio.openWritingPipe(addresses[1]); // ADRESA NA KOJU SE PIŠU PODATCI

  bme.begin(0x76);     // BME280
  Wire.begin();        // I2C
  lightMeter.begin();  // BH1750
  delay(1000);         
}//KRAJ SETUP FUNKCIJE

void loop()  //LOOP 
{
  
  CollectData(); //POZIVA FUNKCIJU KOJA OČITAVA I SPREMA VRIJEDNOSTI SENZORA

  do{
    count++;
    ok=myRadio.write( &data, sizeof(data) ); //  POŠALJI IZMJERENE PODATKE SVE DOK                                         
    delay(100);                              //  VARIJABLA OK NIJE TRUE ILI ODUSTANI NAKON 15 POKUŠAJA
  }while(count<=15 && ok==false);

  count=0;
  delay(5000);                             //  RESETIRAJ BROJ POKUŠAJA, MIRUJ 5 SEKUNDI

}//KRAJ LOOP FUNKCIJE

void CollectData(){                                   // FUNKCIJA KOJA SPREMA IZMJERENE VRIJEDNOSTI SENZORA 
  data.temp=bme.readTemperature();                    // U STRUKTURU VARIJABLE DATA
  data.pres=bme.readPressure() / 100.0F;
  data.alt=bme.readAltitude(SEALEVELPRESSURE_HPA);
  data.hum=bme.readHumidity();
  data.luxe=lightMeter.readLightLevel();
  data.sound=analogRead (soundPin);
  data.ppm=gasSensor.getPPM();
}

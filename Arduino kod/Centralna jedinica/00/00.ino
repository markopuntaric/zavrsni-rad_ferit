//POTREBNE BIBLIOTEKE
#include <SPI.h>  
#include "RF24.h" 

//DEKLARITRATI KONSTANTE
long previousMillis = 0;
const long interval = 20000;


// DEKLARIRATI OBJEKTE I VARIJAVBLE
RF24 myRadio (7, 8); // INSTANCA RADIJA, SPECIFIČNI CE I CS PIN

byte addresses[][6] = {"Node0","Node1","Node2","Node3","Node4"}; // ADRESE CIJEVI ZA SLUŠANJE
 
struct dataReceived{ 
  char a[4];
  float temp;
  float pres;
  float alt;
  float hum;
  float luxe;
  int sound;
  int ppm;
};

dataReceived recdata; // VARIJABLA U KOJU ĆE BITI SPREMLJENI PRISTIGLI PODATCI
dataReceived data[5]; // POLJE STRUKTURA, SLUŽI ZA SPREMANJE PODATAKA IZ SENZORSKIH JEDINICA
 
void setup() //SETUP, OBAVLJA SE SAMO JEDNOM PRILIKOM POKRETANJA
{
  Serial.begin(115200); //INICIJALIZIRAJ SERIAL MONITOR, BRZINA 115200
  myRadio.begin(); 
  myRadio.setChannel(108);  
  myRadio.setPALevel(RF24_PA_MAX); 
  myRadio.setDataRate(RF24_2MBPS); 

  myRadio.openReadingPipe(1, addresses[1]);
  myRadio.openReadingPipe(2, addresses[2]);
  myRadio.openReadingPipe(3, addresses[3]);
  myRadio.openReadingPipe(4, addresses[4]);// POSTAVI ADRESE ZA SLUŠANJE SVAKE SENZORSKE JEDINICE
  myRadio.startListening();
}//KRAJ SETUP FUNKCIJE

void loop()  //LOOP
{
  if ( myRadio.available()) // PROVJERI IMA LI DOSTUPNIH PODATAKA ZA PRIMANJE
  {
    while (myRadio.available())  // SVE DOK IMA 
    {
      myRadio.read( &recdata, sizeof(recdata) ); // PRIMI PODATKE I SPREMI U RECDATA VARIJABLU
    }
     int x=recdata.a[2]-'0';       // RASPOZNAJ IZ KOJE SENZORSKE JEDINICE SU STIGLI PODATCI
     strcpy(data[x].a,recdata.a);  // TE IH SPREMI U ODGOVARAJUĆE POLJE STRUKTURA
     data[x].temp=recdata.temp;
     data[x].pres=recdata.pres;
     data[x].alt=recdata.alt;
     data[x].hum=recdata.hum;
     data[x].luxe=recdata.luxe;
     data[x].sound=recdata.sound;
     data[x].ppm=recdata.ppm;
  }

unsigned long currentMillis = millis();         // PROTEKLO VRIJEME OD POKRETANA PROGRAMA
if(currentMillis - previousMillis > interval) { //AKO JE PROŠAO ZADANI INTERVAL
  SendDatatoRas();
  previousMillis = currentMillis; //RESETIRAJ VREMENSKI INTERVAL
}
}

void SendDatatoRas(){
  for(int i=1;i<=4;i++){            //ŠALJE PODATKE SVAKE SENZORSKE JEDINICE SERIJSKIM PUTEM
    Serial.println(data[i].a);
    Serial.println(data[i].temp);
    Serial.println(data[i].pres);
    Serial.println(data[i].alt);
    Serial.println(data[i].hum);
    Serial.println(data[i].luxe);
    Serial.println(data[i].sound);
    Serial.println(data[i].ppm);
    }
}

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Hidden" color="15" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Changes" color="12" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="diy-modules">
<description>&lt;b&gt;DIY Modules for Arduino, Raspberry Pi, CubieBoard etc.&lt;/b&gt;
&lt;br&gt;&lt;br&gt;
The library contains a list of symbols and footprints for popular, cheap and easy-to-use electronic modules.&lt;br&gt;
The modules are intend to work with microprocessor-based platforms such as &lt;a href="http://arduino.cc"&gt;Arduino&lt;/a&gt;, &lt;a href="http://raspberrypi.org/"&gt;Raspberry Pi&lt;/a&gt;, &lt;a href="http://cubieboard.org/"&gt;CubieBoard&lt;/a&gt;, &lt;a href="http://beagleboard.org/"&gt;BeagleBone&lt;/a&gt; and many others. There are many manufacturers of the modules in the world. Almost all of them can be bought on &lt;a href="ebay.com"&gt;ebay.com&lt;/a&gt;.&lt;br&gt;
&lt;br&gt;
By using this library, you can design a PCB for devices created with usage of modules. Even if you do not need to create PCB design, you can also use the library to quickly document your work by drawing schematics of devices built by you.&lt;br&gt;
&lt;br&gt;
The latest version, examples, photos and much more can be found at: &lt;b&gt;&lt;a href="http://diymodules.org/eagle"&gt;diymodules.org/eagle&lt;/a&gt;&lt;/b&gt;&lt;br&gt;&lt;br&gt;
Comments, suggestions and bug reports please send to: &lt;b&gt;&lt;a href="mailto:eagle@diymodules.org"&gt;eagle@diymodules.org&lt;/b&gt;&lt;/a&gt;&lt;br&gt;&lt;br&gt;
&lt;i&gt;Version: 1.8.0 (2017-Jul-02)&lt;/i&gt;&lt;br&gt;
&lt;i&gt;Created by: Miroslaw Brudnowski&lt;/i&gt;&lt;br&gt;&lt;br&gt;
&lt;i&gt;Released under the Creative Commons Attribution 4.0 International License: &lt;a href="http://creativecommons.org/licenses/by/4.0"&gt;http://creativecommons.org/licenses/by/4.0&lt;/a&gt;&lt;/i&gt;
&lt;br&gt;&lt;br&gt;
&lt;center&gt;
&lt;a href="http://diymodules.org/eagle"&gt;&lt;img src="http://www.diymodules.org/img/diymodules-lbr-image.php?v=1.8.0" alt="DIYmodules.org"&gt;&lt;/a&gt;
&lt;/center&gt;</description>
<packages>
<package name="ARDUINO-NANO-3.0">
<description>&lt;b&gt;Arduino Nano 3.0&lt;/b&gt;</description>
<pad name="J1.1" x="17.78" y="7.62" drill="1" shape="square"/>
<pad name="J1.2" x="15.24" y="7.62" drill="1"/>
<pad name="J1.3" x="12.7" y="7.62" drill="1"/>
<pad name="J1.4" x="10.16" y="7.62" drill="1"/>
<pad name="J1.5" x="7.62" y="7.62" drill="1"/>
<pad name="J1.6" x="5.08" y="7.62" drill="1"/>
<pad name="J1.7" x="2.54" y="7.62" drill="1"/>
<pad name="J1.8" x="0" y="7.62" drill="1"/>
<pad name="J1.9" x="-2.54" y="7.62" drill="1"/>
<pad name="J1.10" x="-5.08" y="7.62" drill="1"/>
<pad name="J1.11" x="-7.62" y="7.62" drill="1"/>
<pad name="J1.12" x="-10.16" y="7.62" drill="1"/>
<wire x1="19.05" y1="8.255" x2="18.415" y2="8.89" width="0.127" layer="21"/>
<wire x1="17.145" y1="8.89" x2="16.51" y2="8.255" width="0.127" layer="21"/>
<wire x1="14.605" y1="8.89" x2="13.97" y2="8.255" width="0.127" layer="21"/>
<wire x1="13.97" y1="8.255" x2="13.335" y2="8.89" width="0.127" layer="21"/>
<wire x1="11.43" y1="8.255" x2="10.795" y2="8.89" width="0.127" layer="21"/>
<wire x1="9.525" y1="8.89" x2="8.89" y2="8.255" width="0.127" layer="21"/>
<wire x1="8.89" y1="8.255" x2="8.255" y2="8.89" width="0.127" layer="21"/>
<wire x1="6.985" y1="8.89" x2="6.35" y2="8.255" width="0.127" layer="21"/>
<wire x1="6.35" y1="8.255" x2="5.715" y2="8.89" width="0.127" layer="21"/>
<wire x1="4.445" y1="8.89" x2="3.81" y2="8.255" width="0.127" layer="21"/>
<wire x1="3.81" y1="8.255" x2="3.175" y2="8.89" width="0.127" layer="21"/>
<wire x1="1.905" y1="8.89" x2="1.27" y2="8.255" width="0.127" layer="21"/>
<wire x1="1.27" y1="8.255" x2="0.635" y2="8.89" width="0.127" layer="21"/>
<wire x1="-0.635" y1="8.89" x2="-1.27" y2="8.255" width="0.127" layer="21"/>
<wire x1="-1.27" y1="8.255" x2="-1.905" y2="8.89" width="0.127" layer="21"/>
<wire x1="-3.175" y1="8.89" x2="-3.81" y2="8.255" width="0.127" layer="21"/>
<wire x1="-5.715" y1="8.89" x2="-6.35" y2="8.255" width="0.127" layer="21"/>
<wire x1="-6.35" y1="8.255" x2="-6.985" y2="8.89" width="0.127" layer="21"/>
<wire x1="-8.255" y1="8.89" x2="-8.89" y2="8.255" width="0.127" layer="21"/>
<wire x1="-8.89" y1="8.255" x2="-9.525" y2="8.89" width="0.127" layer="21"/>
<wire x1="-10.795" y1="8.89" x2="-11.43" y2="8.255" width="0.127" layer="21"/>
<wire x1="-11.43" y1="6.985" x2="-10.795" y2="6.35" width="0.127" layer="21"/>
<wire x1="-10.795" y1="6.35" x2="-9.525" y2="6.35" width="0.127" layer="21"/>
<wire x1="-9.525" y1="6.35" x2="-8.89" y2="6.985" width="0.127" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.255" y2="6.35" width="0.127" layer="21"/>
<wire x1="-8.255" y1="6.35" x2="-6.985" y2="6.35" width="0.127" layer="21"/>
<wire x1="-6.985" y1="6.35" x2="-6.35" y2="6.985" width="0.127" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-5.715" y2="6.35" width="0.127" layer="21"/>
<wire x1="-5.715" y1="6.35" x2="-4.445" y2="6.35" width="0.127" layer="21"/>
<wire x1="-4.445" y1="6.35" x2="-3.81" y2="6.985" width="0.127" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.175" y2="6.35" width="0.127" layer="21"/>
<wire x1="-3.175" y1="6.35" x2="-1.905" y2="6.35" width="0.127" layer="21"/>
<wire x1="-1.905" y1="6.35" x2="-1.27" y2="6.985" width="0.127" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-0.635" y2="6.35" width="0.127" layer="21"/>
<wire x1="-0.635" y1="6.35" x2="0.635" y2="6.35" width="0.127" layer="21"/>
<wire x1="0.635" y1="6.35" x2="1.27" y2="6.985" width="0.127" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.905" y2="6.35" width="0.127" layer="21"/>
<wire x1="1.905" y1="6.35" x2="3.175" y2="6.35" width="0.127" layer="21"/>
<wire x1="3.175" y1="6.35" x2="3.81" y2="6.985" width="0.127" layer="21"/>
<wire x1="3.81" y1="6.985" x2="4.445" y2="6.35" width="0.127" layer="21"/>
<wire x1="4.445" y1="6.35" x2="5.715" y2="6.35" width="0.127" layer="21"/>
<wire x1="5.715" y1="6.35" x2="6.35" y2="6.985" width="0.127" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.985" y2="6.35" width="0.127" layer="21"/>
<wire x1="6.985" y1="6.35" x2="8.255" y2="6.35" width="0.127" layer="21"/>
<wire x1="8.255" y1="6.35" x2="8.89" y2="6.985" width="0.127" layer="21"/>
<wire x1="8.89" y1="6.985" x2="9.525" y2="6.35" width="0.127" layer="21"/>
<wire x1="9.525" y1="6.35" x2="10.795" y2="6.35" width="0.127" layer="21"/>
<wire x1="10.795" y1="6.35" x2="11.43" y2="6.985" width="0.127" layer="21"/>
<wire x1="11.43" y1="6.985" x2="12.065" y2="6.35" width="0.127" layer="21"/>
<wire x1="12.065" y1="6.35" x2="13.335" y2="6.35" width="0.127" layer="21"/>
<wire x1="13.335" y1="6.35" x2="13.97" y2="6.985" width="0.127" layer="21"/>
<wire x1="13.97" y1="6.985" x2="14.605" y2="6.35" width="0.127" layer="21"/>
<wire x1="14.605" y1="6.35" x2="15.875" y2="6.35" width="0.127" layer="21"/>
<wire x1="15.875" y1="6.35" x2="16.51" y2="6.985" width="0.127" layer="21"/>
<wire x1="16.51" y1="6.985" x2="17.145" y2="6.35" width="0.127" layer="21"/>
<wire x1="17.145" y1="6.35" x2="18.415" y2="6.35" width="0.127" layer="21"/>
<wire x1="18.415" y1="6.35" x2="19.05" y2="6.985" width="0.127" layer="21"/>
<pad name="J2.1" x="17.78" y="-7.62" drill="1"/>
<pad name="J2.2" x="15.24" y="-7.62" drill="1"/>
<pad name="J2.3" x="12.7" y="-7.62" drill="1"/>
<pad name="J2.4" x="10.16" y="-7.62" drill="1"/>
<pad name="J2.5" x="7.62" y="-7.62" drill="1"/>
<pad name="J2.6" x="5.08" y="-7.62" drill="1"/>
<pad name="J2.8" x="0" y="-7.62" drill="1"/>
<pad name="J2.9" x="-2.54" y="-7.62" drill="1"/>
<pad name="J2.11" x="-7.62" y="-7.62" drill="1"/>
<pad name="J2.12" x="-10.16" y="-7.62" drill="1"/>
<wire x1="19.05" y1="-6.985" x2="18.415" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-10.795" y1="-6.35" x2="-11.43" y2="-6.985" width="0.127" layer="21"/>
<wire x1="18.415" y1="-8.89" x2="19.05" y2="-8.255" width="0.127" layer="21"/>
<wire x1="-10.795" y1="-8.89" x2="-11.43" y2="-8.255" width="0.127" layer="21"/>
<pad name="J2.10" x="-5.08" y="-7.62" drill="1"/>
<pad name="J2.7" x="2.54" y="-7.62" drill="1"/>
<wire x1="21.59" y1="8.89" x2="15.875" y2="8.89" width="0.127" layer="21"/>
<wire x1="15.875" y1="8.89" x2="12.065" y2="8.89" width="0.127" layer="21"/>
<wire x1="12.065" y1="8.89" x2="-4.445" y2="8.89" width="0.127" layer="21"/>
<wire x1="-9.525" y1="-8.89" x2="-8.255" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-8.89" x2="-6.985" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-8.89" x2="-5.715" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-8.89" x2="-4.445" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-8.89" x2="-3.175" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-8.89" x2="-1.905" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-8.89" x2="-0.635" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-8.89" x2="0.635" y2="-8.89" width="0.127" layer="21"/>
<wire x1="0.635" y1="-8.89" x2="1.905" y2="-8.89" width="0.127" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="3.175" y2="-8.89" width="0.127" layer="21"/>
<wire x1="3.175" y1="-8.89" x2="4.445" y2="-8.89" width="0.127" layer="21"/>
<wire x1="4.445" y1="-8.89" x2="5.715" y2="-8.89" width="0.127" layer="21"/>
<wire x1="5.715" y1="-8.89" x2="6.985" y2="-8.89" width="0.127" layer="21"/>
<wire x1="6.985" y1="-8.89" x2="8.255" y2="-8.89" width="0.127" layer="21"/>
<wire x1="8.255" y1="-8.89" x2="9.525" y2="-8.89" width="0.127" layer="21"/>
<wire x1="9.525" y1="-8.89" x2="10.795" y2="-8.89" width="0.127" layer="21"/>
<wire x1="10.795" y1="-8.89" x2="12.065" y2="-8.89" width="0.127" layer="21"/>
<wire x1="12.065" y1="-8.89" x2="13.335" y2="-8.89" width="0.127" layer="21"/>
<wire x1="13.335" y1="-8.89" x2="14.605" y2="-8.89" width="0.127" layer="21"/>
<wire x1="14.605" y1="-8.89" x2="15.875" y2="-8.89" width="0.127" layer="21"/>
<wire x1="15.875" y1="-8.89" x2="17.145" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-3.81" y1="8.255" x2="-4.445" y2="8.89" width="0.127" layer="21"/>
<wire x1="16.51" y1="8.255" x2="15.875" y2="8.89" width="0.127" layer="21"/>
<wire x1="11.43" y1="8.255" x2="12.065" y2="8.89" width="0.127" layer="21"/>
<wire x1="-9.525" y1="-8.89" x2="-8.89" y2="-8.255" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-8.255" x2="-8.255" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-8.89" x2="-6.35" y2="-8.255" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-8.255" x2="-5.715" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-8.89" x2="-3.81" y2="-8.255" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-8.255" x2="-3.175" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-8.89" x2="-1.27" y2="-8.255" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-8.255" x2="-0.635" y2="-8.89" width="0.127" layer="21"/>
<wire x1="0.635" y1="-8.89" x2="1.27" y2="-8.255" width="0.127" layer="21"/>
<wire x1="1.27" y1="-8.255" x2="1.905" y2="-8.89" width="0.127" layer="21"/>
<wire x1="3.175" y1="-8.89" x2="3.81" y2="-8.255" width="0.127" layer="21"/>
<wire x1="3.81" y1="-8.255" x2="4.445" y2="-8.89" width="0.127" layer="21"/>
<wire x1="5.715" y1="-8.89" x2="6.35" y2="-8.255" width="0.127" layer="21"/>
<wire x1="6.35" y1="-8.255" x2="6.985" y2="-8.89" width="0.127" layer="21"/>
<wire x1="8.255" y1="-8.89" x2="8.89" y2="-8.255" width="0.127" layer="21"/>
<wire x1="8.89" y1="-8.255" x2="9.525" y2="-8.89" width="0.127" layer="21"/>
<wire x1="10.795" y1="-8.89" x2="11.43" y2="-8.255" width="0.127" layer="21"/>
<wire x1="11.43" y1="-8.255" x2="12.065" y2="-8.89" width="0.127" layer="21"/>
<wire x1="13.335" y1="-8.89" x2="13.97" y2="-8.255" width="0.127" layer="21"/>
<wire x1="13.97" y1="-8.255" x2="14.605" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-10.795" y1="-6.35" x2="-9.525" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-9.525" y1="-6.35" x2="-8.89" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-6.985" x2="-8.255" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-6.35" x2="-6.985" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-6.35" x2="-6.35" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-6.985" x2="-5.715" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-6.35" x2="-4.445" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-6.35" x2="-3.81" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-6.985" x2="-3.175" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-6.35" x2="-1.905" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-6.35" x2="-1.27" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-6.985" x2="-0.635" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-6.35" x2="0.635" y2="-6.35" width="0.127" layer="21"/>
<wire x1="0.635" y1="-6.35" x2="1.27" y2="-6.985" width="0.127" layer="21"/>
<wire x1="1.27" y1="-6.985" x2="1.905" y2="-6.35" width="0.127" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="3.175" y2="-6.35" width="0.127" layer="21"/>
<wire x1="3.175" y1="-6.35" x2="3.81" y2="-6.985" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="4.445" y2="-6.35" width="0.127" layer="21"/>
<wire x1="4.445" y1="-6.35" x2="5.715" y2="-6.35" width="0.127" layer="21"/>
<wire x1="5.715" y1="-6.35" x2="6.35" y2="-6.985" width="0.127" layer="21"/>
<wire x1="6.35" y1="-6.985" x2="6.985" y2="-6.35" width="0.127" layer="21"/>
<wire x1="6.985" y1="-6.35" x2="8.255" y2="-6.35" width="0.127" layer="21"/>
<wire x1="8.255" y1="-6.35" x2="8.89" y2="-6.985" width="0.127" layer="21"/>
<wire x1="8.89" y1="-6.985" x2="9.525" y2="-6.35" width="0.127" layer="21"/>
<wire x1="9.525" y1="-6.35" x2="10.795" y2="-6.35" width="0.127" layer="21"/>
<wire x1="10.795" y1="-6.35" x2="11.43" y2="-6.985" width="0.127" layer="21"/>
<wire x1="11.43" y1="-6.985" x2="12.065" y2="-6.35" width="0.127" layer="21"/>
<wire x1="12.065" y1="-6.35" x2="13.335" y2="-6.35" width="0.127" layer="21"/>
<wire x1="13.335" y1="-6.35" x2="13.97" y2="-6.985" width="0.127" layer="21"/>
<wire x1="13.97" y1="-6.985" x2="14.605" y2="-6.35" width="0.127" layer="21"/>
<wire x1="14.605" y1="-6.35" x2="15.875" y2="-6.35" width="0.127" layer="21"/>
<wire x1="15.875" y1="-6.35" x2="16.51" y2="-6.985" width="0.127" layer="21"/>
<wire x1="16.51" y1="-6.985" x2="17.145" y2="-6.35" width="0.127" layer="21"/>
<wire x1="17.145" y1="-6.35" x2="18.415" y2="-6.35" width="0.127" layer="21"/>
<wire x1="17.145" y1="-8.89" x2="16.51" y2="-8.255" width="0.127" layer="21"/>
<wire x1="16.51" y1="-8.255" x2="15.875" y2="-8.89" width="0.127" layer="21"/>
<text x="0" y="10.16" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-10.16" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<pad name="J1.13" x="-12.7" y="7.62" drill="1"/>
<pad name="J1.14" x="-15.24" y="7.62" drill="1"/>
<pad name="J1.15" x="-17.78" y="7.62" drill="1"/>
<pad name="J2.13" x="-12.7" y="-7.62" drill="1"/>
<pad name="J2.14" x="-15.24" y="-7.62" drill="1"/>
<pad name="J2.15" x="-17.78" y="-7.62" drill="1"/>
<hole x="-20.32" y="7.62" drill="1.651"/>
<hole x="-20.32" y="-7.62" drill="1.651"/>
<hole x="20.32" y="-7.62" drill="1.651"/>
<hole x="20.32" y="7.62" drill="1.651"/>
<wire x1="-4.445" y1="8.89" x2="-12.065" y2="8.89" width="0.127" layer="21"/>
<wire x1="-12.065" y1="8.89" x2="-13.335" y2="8.89" width="0.127" layer="21"/>
<wire x1="-13.335" y1="8.89" x2="-14.605" y2="8.89" width="0.127" layer="21"/>
<wire x1="-14.605" y1="8.89" x2="-15.875" y2="8.89" width="0.127" layer="21"/>
<wire x1="-15.875" y1="8.89" x2="-17.145" y2="8.89" width="0.127" layer="21"/>
<wire x1="-17.145" y1="8.89" x2="-18.415" y2="8.89" width="0.127" layer="21"/>
<wire x1="-18.415" y1="8.89" x2="-21.59" y2="8.89" width="0.127" layer="21"/>
<wire x1="-21.59" y1="8.89" x2="-21.59" y2="3.175" width="0.127" layer="21"/>
<wire x1="-21.59" y1="-3.175" x2="-21.59" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-21.59" y1="-8.89" x2="-18.415" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-18.415" y1="-8.89" x2="-17.145" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-17.145" y1="-8.89" x2="-15.875" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-8.89" x2="-14.605" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-14.605" y1="-8.89" x2="-13.335" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-8.89" x2="-12.065" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-12.065" y1="-8.89" x2="-9.525" y2="-8.89" width="0.127" layer="21"/>
<wire x1="21.59" y1="8.89" x2="21.59" y2="3.175" width="0.127" layer="21"/>
<wire x1="21.59" y1="3.175" x2="21.59" y2="-3.175" width="0.127" layer="21"/>
<wire x1="21.59" y1="-3.175" x2="21.59" y2="-8.89" width="0.127" layer="21"/>
<wire x1="21.59" y1="-8.89" x2="17.145" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-11.43" y1="8.255" x2="-12.065" y2="8.89" width="0.127" layer="21"/>
<wire x1="-13.335" y1="8.89" x2="-13.97" y2="8.255" width="0.127" layer="21"/>
<wire x1="-13.97" y1="8.255" x2="-14.605" y2="8.89" width="0.127" layer="21"/>
<wire x1="-15.875" y1="8.89" x2="-16.51" y2="8.255" width="0.127" layer="21"/>
<wire x1="-16.51" y1="8.255" x2="-17.145" y2="8.89" width="0.127" layer="21"/>
<wire x1="-18.415" y1="8.89" x2="-19.05" y2="8.255" width="0.127" layer="21"/>
<wire x1="-19.05" y1="8.255" x2="-19.05" y2="6.985" width="0.127" layer="21"/>
<wire x1="-19.05" y1="6.985" x2="-18.415" y2="6.35" width="0.127" layer="21"/>
<wire x1="-18.415" y1="6.35" x2="-17.145" y2="6.35" width="0.127" layer="21"/>
<wire x1="-17.145" y1="6.35" x2="-16.51" y2="6.985" width="0.127" layer="21"/>
<wire x1="-16.51" y1="6.985" x2="-15.875" y2="6.35" width="0.127" layer="21"/>
<wire x1="-15.875" y1="6.35" x2="-14.605" y2="6.35" width="0.127" layer="21"/>
<wire x1="-14.605" y1="6.35" x2="-13.97" y2="6.985" width="0.127" layer="21"/>
<wire x1="-13.97" y1="6.985" x2="-13.335" y2="6.35" width="0.127" layer="21"/>
<wire x1="-13.335" y1="6.35" x2="-12.065" y2="6.35" width="0.127" layer="21"/>
<wire x1="-12.065" y1="6.35" x2="-11.43" y2="6.985" width="0.127" layer="21"/>
<wire x1="19.05" y1="8.255" x2="19.05" y2="6.985" width="0.127" layer="21"/>
<wire x1="19.05" y1="-6.985" x2="19.05" y2="-8.255" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-6.985" x2="-12.065" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-12.065" y1="-6.35" x2="-13.335" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-6.35" x2="-13.97" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-6.985" x2="-14.605" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-14.605" y1="-6.35" x2="-15.875" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-6.35" x2="-16.51" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-6.985" x2="-17.145" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-17.145" y1="-6.35" x2="-18.415" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-18.415" y1="-6.35" x2="-19.05" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-6.985" x2="-19.05" y2="-8.255" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-8.255" x2="-18.415" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-17.145" y1="-8.89" x2="-16.51" y2="-8.255" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-8.255" x2="-15.875" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-14.605" y1="-8.89" x2="-13.97" y2="-8.255" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-8.255" x2="-13.335" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-12.065" y1="-8.89" x2="-11.43" y2="-8.255" width="0.127" layer="21"/>
<wire x1="-21.59" y1="3.175" x2="-13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="-13.97" y1="3.175" x2="-13.97" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-3.175" x2="-21.59" y2="-3.175" width="0.127" layer="21"/>
<text x="-17.78" y="0" size="1.778" layer="21" align="center">USB</text>
<wire x1="-21.59" y1="3.175" x2="-22.86" y2="3.175" width="0.127" layer="21"/>
<wire x1="-22.86" y1="3.175" x2="-22.86" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-22.86" y1="-3.175" x2="-21.59" y2="-3.175" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<circle x="2.54" y="-0.635" radius="0.762" width="0.127" layer="21"/>
<text x="5.08" y="-0.635" size="1.016" layer="21" rot="R90" align="center">RESET</text>
</package>
<package name="GAS-SENSOR-MQ2">
<description>&lt;b&gt;Gas Sensor&lt;/b&gt; based on &lt;b&gt;MQ-2&lt;/b&gt; device</description>
<wire x1="-16.256" y1="10.16" x2="16.256" y2="10.16" width="0.127" layer="21"/>
<wire x1="16.256" y1="10.16" x2="16.256" y2="-10.16" width="0.127" layer="21"/>
<wire x1="16.256" y1="-10.16" x2="-16.256" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-16.256" y1="-10.16" x2="-16.256" y2="10.16" width="0.127" layer="21"/>
<pad name="1" x="-14.605" y="3.81" drill="1" shape="square"/>
<pad name="2" x="-14.605" y="1.27" drill="1"/>
<pad name="3" x="-14.605" y="-1.27" drill="1"/>
<circle x="3.81" y="0" radius="9.779" width="0.127" layer="21"/>
<wire x1="-15.875" y1="4.445" x2="-15.24" y2="5.08" width="0.127" layer="21"/>
<wire x1="-15.24" y1="5.08" x2="-13.97" y2="5.08" width="0.127" layer="21"/>
<wire x1="-13.97" y1="5.08" x2="-13.335" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.335" y1="4.445" x2="-13.335" y2="3.175" width="0.127" layer="21"/>
<wire x1="-13.335" y1="3.175" x2="-13.97" y2="2.54" width="0.127" layer="21"/>
<wire x1="-13.97" y1="2.54" x2="-13.335" y2="1.905" width="0.127" layer="21"/>
<wire x1="-13.335" y1="1.905" x2="-13.335" y2="0.635" width="0.127" layer="21"/>
<wire x1="-13.335" y1="0.635" x2="-13.97" y2="0" width="0.127" layer="21"/>
<wire x1="-13.97" y1="0" x2="-13.335" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-0.635" x2="-13.335" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-1.905" x2="-13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-15.24" y1="-2.54" x2="-15.875" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-1.905" x2="-15.875" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-0.635" x2="-15.24" y2="0" width="0.127" layer="21"/>
<wire x1="-15.24" y1="0" x2="-15.875" y2="0.635" width="0.127" layer="21"/>
<wire x1="-15.875" y1="0.635" x2="-15.875" y2="1.905" width="0.127" layer="21"/>
<wire x1="-15.875" y1="1.905" x2="-15.24" y2="2.54" width="0.127" layer="21"/>
<wire x1="-15.24" y1="2.54" x2="-15.875" y2="3.175" width="0.127" layer="21"/>
<wire x1="-15.875" y1="3.175" x2="-15.875" y2="4.445" width="0.127" layer="21"/>
<text x="0" y="11.43" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-11.43" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<pad name="4" x="-14.605" y="-3.81" drill="1"/>
<wire x1="-15.24" y1="-2.54" x2="-15.875" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-3.175" x2="-15.875" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-4.445" x2="-15.24" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-15.24" y1="-5.08" x2="-13.97" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-5.08" x2="-13.335" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-4.445" x2="-13.335" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-3.175" x2="-13.97" y2="-2.54" width="0.127" layer="21"/>
<hole x="-13.589" y="7.62" drill="2.8"/>
<hole x="-13.589" y="-7.62" drill="2.8"/>
<hole x="13.589" y="-7.62" drill="2.8"/>
<hole x="13.589" y="7.62" drill="2.8"/>
<text x="3.81" y="0" size="1.778" layer="21" align="center">SENSOR</text>
</package>
<package name="LIGHT-SENSOR-BH1750">
<description>&lt;b&gt;Digital Light Sensor&lt;/b&gt; based on &lt;b&gt;BH1750&lt;/b&gt;</description>
<wire x1="-8.255" y1="6.35" x2="-6.985" y2="6.35" width="0.127" layer="21"/>
<text x="0" y="8.255" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-8.255" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<pad name="1" x="-7.62" y="-5.08" drill="1" shape="square"/>
<pad name="2" x="-7.62" y="-2.54" drill="1"/>
<pad name="3" x="-7.62" y="0" drill="1"/>
<pad name="4" x="-7.62" y="2.54" drill="1"/>
<wire x1="-6.985" y1="6.35" x2="-6.35" y2="5.715" width="0.127" layer="21"/>
<wire x1="-6.35" y1="5.715" x2="-6.35" y2="4.445" width="0.127" layer="21"/>
<wire x1="-6.35" y1="4.445" x2="-6.985" y2="3.81" width="0.127" layer="21"/>
<wire x1="-6.985" y1="3.81" x2="-6.35" y2="3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="3.175" x2="-6.35" y2="1.905" width="0.127" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-6.985" y2="1.27" width="0.127" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-6.35" y2="0.635" width="0.127" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-6.985" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-6.35" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-6.35" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.175" x2="-6.985" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-8.89" y1="5.715" x2="-8.255" y2="6.35" width="0.127" layer="21"/>
<wire x1="-8.89" y1="4.445" x2="-8.255" y2="3.81" width="0.127" layer="21"/>
<wire x1="-8.255" y1="3.81" x2="-8.89" y2="3.175" width="0.127" layer="21"/>
<wire x1="-8.89" y1="1.905" x2="-8.255" y2="1.27" width="0.127" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-8.89" y2="0.635" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-0.635" x2="-8.255" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-8.89" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-3.175" x2="-8.255" y2="-3.81" width="0.127" layer="21"/>
<text x="-5.715" y="0" size="1.27" layer="21" align="center-left">SCL</text>
<wire x1="-9.652" y1="7.112" x2="9.652" y2="7.112" width="0.127" layer="21"/>
<wire x1="9.652" y1="7.112" x2="9.652" y2="-7.112" width="0.127" layer="21"/>
<wire x1="9.652" y1="-7.112" x2="-9.652" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-9.652" y1="-7.112" x2="-9.652" y2="7.112" width="0.127" layer="21"/>
<wire x1="-8.89" y1="5.715" x2="-8.89" y2="4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="3.175" x2="-8.89" y2="1.905" width="0.127" layer="21"/>
<wire x1="-8.89" y1="0.635" x2="-8.89" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-1.905" x2="-8.89" y2="-3.175" width="0.127" layer="21"/>
<text x="-5.715" y="2.54" size="1.27" layer="21" align="center-left">SDA</text>
<text x="-5.715" y="5.08" size="1.27" layer="21" align="center-left">ADDR</text>
<text x="-5.715" y="-2.54" size="1.27" layer="21" align="center-left">GND</text>
<hole x="6.35" y="4.445" drill="3"/>
<wire x1="1.27" y1="1.27" x2="2.794" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.794" y1="1.27" x2="2.794" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.794" y1="-1.27" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="5" x="-7.62" y="5.08" drill="1"/>
<wire x1="-8.255" y1="-3.81" x2="-8.89" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-4.445" x2="-8.89" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-5.715" x2="-8.255" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-6.35" x2="-6.985" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-6.35" x2="-6.35" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-5.715" x2="-6.35" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-4.445" x2="-6.985" y2="-3.81" width="0.127" layer="21"/>
<hole x="6.35" y="-4.445" drill="3"/>
<text x="-5.715" y="-5.08" size="1.27" layer="21" align="center-left">VCC</text>
<text x="3.81" y="0" size="1.016" layer="21" align="center-left">BH1750</text>
</package>
<package name="SOUND-DETECTOR">
<description>&lt;b&gt;Sound Detection Module&lt;/b&gt;</description>
<wire x1="-16.256" y1="8.255" x2="16.256" y2="8.255" width="0.127" layer="21"/>
<wire x1="16.256" y1="8.255" x2="16.256" y2="-8.255" width="0.127" layer="21"/>
<wire x1="16.256" y1="-8.255" x2="-16.256" y2="-8.255" width="0.127" layer="21"/>
<wire x1="-16.256" y1="-8.255" x2="-16.256" y2="8.255" width="0.127" layer="21"/>
<pad name="1" x="-14.605" y="2.54" drill="1" shape="square"/>
<pad name="2" x="-14.605" y="0" drill="1"/>
<pad name="3" x="-14.605" y="-2.54" drill="1"/>
<hole x="-14.5" y="6.5" drill="1"/>
<hole x="-14.5" y="-6.5" drill="1"/>
<hole x="14.5" y="-6.5" drill="1"/>
<hole x="14.5" y="6.5" drill="1"/>
<circle x="10.16" y="2.54" radius="5" width="0.127" layer="21"/>
<text x="10.16" y="2.54" size="1.778" layer="21" align="center">MIC</text>
<wire x1="-15.875" y1="3.175" x2="-15.24" y2="3.81" width="0.127" layer="21"/>
<wire x1="-15.24" y1="3.81" x2="-13.97" y2="3.81" width="0.127" layer="21"/>
<wire x1="-13.97" y1="3.81" x2="-13.335" y2="3.175" width="0.127" layer="21"/>
<wire x1="-13.335" y1="3.175" x2="-13.335" y2="1.905" width="0.127" layer="21"/>
<wire x1="-13.335" y1="1.905" x2="-13.97" y2="1.27" width="0.127" layer="21"/>
<wire x1="-13.97" y1="1.27" x2="-13.335" y2="0.635" width="0.127" layer="21"/>
<wire x1="-13.335" y1="0.635" x2="-13.335" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-0.635" x2="-13.97" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-1.27" x2="-13.335" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-1.905" x2="-13.335" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-3.175" x2="-13.97" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-3.81" x2="-15.24" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-15.24" y1="-3.81" x2="-15.875" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-3.175" x2="-15.875" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-1.905" x2="-15.24" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-15.24" y1="-1.27" x2="-15.875" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-0.635" x2="-15.875" y2="0.635" width="0.127" layer="21"/>
<wire x1="-15.875" y1="0.635" x2="-15.24" y2="1.27" width="0.127" layer="21"/>
<wire x1="-15.24" y1="1.27" x2="-15.875" y2="1.905" width="0.127" layer="21"/>
<wire x1="-15.875" y1="1.905" x2="-15.875" y2="3.175" width="0.127" layer="21"/>
<text x="0" y="8.89" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-8.89" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="TEMP-HUM-PRES-BME280">
<description>&lt;b&gt;Digital Humidity-Temperature-Pressure Sensor&lt;/b&gt; based on &lt;b&gt;BME280&lt;/b&gt; device</description>
<wire x1="-5.715" y1="5.08" x2="-4.445" y2="5.08" width="0.127" layer="21"/>
<text x="0" y="6.35" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-6.35" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<pad name="1" x="-5.08" y="3.81" drill="1" shape="square"/>
<pad name="2" x="-5.08" y="1.27" drill="1"/>
<pad name="3" x="-5.08" y="-1.27" drill="1"/>
<pad name="4" x="-5.08" y="-3.81" drill="1"/>
<wire x1="-4.445" y1="5.08" x2="-3.81" y2="4.445" width="0.127" layer="21"/>
<wire x1="-3.81" y1="4.445" x2="-3.81" y2="3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="3.175" x2="-4.445" y2="2.54" width="0.127" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.81" y2="1.905" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="0.635" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-4.445" y2="0" width="0.127" layer="21"/>
<wire x1="-4.445" y1="0" x2="-3.81" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.81" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-4.445" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.81" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.175" x2="-3.81" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-4.445" x2="-4.445" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-6.35" y1="4.445" x2="-5.715" y2="5.08" width="0.127" layer="21"/>
<wire x1="-6.35" y1="3.175" x2="-5.715" y2="2.54" width="0.127" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-6.35" y2="1.905" width="0.127" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-5.715" y2="0" width="0.127" layer="21"/>
<wire x1="-5.715" y1="0" x2="-6.35" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-5.715" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.35" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-4.445" x2="-5.715" y2="-5.08" width="0.127" layer="21"/>
<text x="-3.175" y="-1.27" size="1.27" layer="21" align="center-left">SCL</text>
<wire x1="-6.604" y1="5.334" x2="6.604" y2="5.334" width="0.127" layer="21"/>
<wire x1="6.604" y1="5.334" x2="6.604" y2="-5.334" width="0.127" layer="21"/>
<wire x1="6.604" y1="-5.334" x2="-6.604" y2="-5.334" width="0.127" layer="21"/>
<wire x1="-6.604" y1="-5.334" x2="-6.604" y2="5.334" width="0.127" layer="21"/>
<wire x1="-6.35" y1="4.445" x2="-6.35" y2="3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-6.35" y2="0.635" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-6.35" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.175" x2="-6.35" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-5.08" x2="-4.445" y2="-5.08" width="0.127" layer="21"/>
<text x="-3.175" y="1.27" size="1.27" layer="21" align="center-left">GND</text>
<text x="-3.175" y="3.81" size="1.27" layer="21" align="center-left">VIN</text>
<text x="-3.175" y="-3.81" size="1.27" layer="21" align="center-left">SDA</text>
<hole x="3.81" y="2.54" drill="3"/>
<wire x1="2.54" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-3.81" width="0.127" layer="21"/>
<wire x1="5.08" y1="-3.81" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<circle x="3.048" y="-2.54" radius="0.254" width="0.127" layer="21"/>
</package>
<package name="WIRELESS-NRF24L01">
<description>&lt;b&gt;2.4 GHz Wireless Module&lt;/b&gt; based on &lt;b&gt;NRF24L01&lt;/b&gt; chip</description>
<pad name="1" x="-12.7" y="6.35" drill="1" shape="square"/>
<pad name="8" x="-10.16" y="-1.27" drill="1"/>
<pad name="6" x="-10.16" y="1.27" drill="1"/>
<pad name="4" x="-10.16" y="3.81" drill="1"/>
<pad name="2" x="-10.16" y="6.35" drill="1"/>
<pad name="3" x="-12.7" y="3.81" drill="1"/>
<pad name="5" x="-12.7" y="1.27" drill="1"/>
<pad name="7" x="-12.7" y="-1.27" drill="1"/>
<text x="0" y="8.89" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-7.62" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<wire x1="-14.3764" y1="8.001" x2="8.128" y2="8.001" width="0.127" layer="21"/>
<wire x1="8.128" y1="8.001" x2="14.6304" y2="8.001" width="0.127" layer="21"/>
<wire x1="14.6304" y1="8.001" x2="14.6304" y2="-6.985" width="0.127" layer="21"/>
<wire x1="14.6304" y1="-6.985" x2="8.128" y2="-6.985" width="0.127" layer="21"/>
<wire x1="8.128" y1="-6.985" x2="-14.3764" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-14.3764" y1="-6.985" x2="-14.3764" y2="8.001" width="0.127" layer="21"/>
<wire x1="-13.97" y1="6.985" x2="-13.335" y2="7.62" width="0.127" layer="21"/>
<wire x1="-13.335" y1="7.62" x2="-9.525" y2="7.62" width="0.127" layer="21"/>
<wire x1="-9.525" y1="7.62" x2="-8.89" y2="6.985" width="0.127" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="5.715" width="0.127" layer="21"/>
<wire x1="-8.89" y1="5.715" x2="-9.525" y2="5.08" width="0.127" layer="21"/>
<wire x1="-9.525" y1="5.08" x2="-8.89" y2="4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="4.445" x2="-8.89" y2="3.175" width="0.127" layer="21"/>
<wire x1="-8.89" y1="3.175" x2="-9.525" y2="2.54" width="0.127" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.89" y2="1.905" width="0.127" layer="21"/>
<wire x1="-8.89" y1="1.905" x2="-8.89" y2="0.635" width="0.127" layer="21"/>
<wire x1="-8.89" y1="0.635" x2="-9.525" y2="0" width="0.127" layer="21"/>
<wire x1="-9.525" y1="0" x2="-8.89" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-0.635" x2="-8.89" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-1.905" x2="-9.525" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-9.525" y1="-2.54" x2="-10.795" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-11.43" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-1.905" x2="-12.065" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-13.335" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.97" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-1.905" x2="-13.97" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-0.635" x2="-13.335" y2="0" width="0.127" layer="21"/>
<wire x1="-13.335" y1="0" x2="-13.97" y2="0.635" width="0.127" layer="21"/>
<wire x1="-13.97" y1="0.635" x2="-13.97" y2="1.905" width="0.127" layer="21"/>
<wire x1="-13.97" y1="1.905" x2="-13.335" y2="2.54" width="0.127" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="-13.97" y1="3.175" x2="-13.97" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.445" x2="-13.335" y2="5.08" width="0.127" layer="21"/>
<wire x1="-13.335" y1="5.08" x2="-13.97" y2="5.715" width="0.127" layer="21"/>
<wire x1="-13.97" y1="5.715" x2="-13.97" y2="6.985" width="0.127" layer="21"/>
<wire x1="8.128" y1="8.001" x2="8.128" y2="-6.985" width="0.127" layer="21"/>
<text x="12.192" y="0.508" size="1.778" layer="21" rot="R90" align="bottom-center">ANTENNA</text>
</package>
<package name="BREADBOARD-POWER-V1">
<description>&lt;b&gt;Breadboard Power Adapter&lt;/b&gt; with onboard 3.3V and 5V regulators</description>
<wire x1="-24.765" y1="16.256" x2="-24.765" y2="13.335" width="0.127" layer="21"/>
<wire x1="-24.765" y1="13.335" x2="-24.765" y2="12.065" width="0.127" layer="21"/>
<wire x1="-24.765" y1="12.065" x2="-24.765" y2="10.795" width="0.127" layer="21"/>
<wire x1="-24.765" y1="10.795" x2="-24.765" y2="9.525" width="0.127" layer="21"/>
<wire x1="-24.765" y1="9.525" x2="-24.765" y2="8.255" width="0.127" layer="21"/>
<wire x1="-24.765" y1="8.255" x2="-24.765" y2="6.985" width="0.127" layer="21"/>
<wire x1="-24.765" y1="6.985" x2="-24.765" y2="5.715" width="0.127" layer="21"/>
<wire x1="-24.765" y1="5.715" x2="-24.765" y2="4.445" width="0.127" layer="21"/>
<wire x1="-24.765" y1="4.445" x2="-24.765" y2="3.175" width="0.127" layer="21"/>
<wire x1="-24.765" y1="3.175" x2="-24.765" y2="-16.256" width="0.127" layer="21"/>
<wire x1="-24.765" y1="-16.256" x2="-24.13" y2="-16.256" width="0.127" layer="21"/>
<wire x1="-24.13" y1="-16.256" x2="-20.574" y2="-16.256" width="0.127" layer="21"/>
<wire x1="-20.574" y1="-16.256" x2="-13.97" y2="-16.256" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-16.256" x2="-6.35" y2="-16.256" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-16.256" x2="7.874" y2="-16.256" width="0.127" layer="21"/>
<wire x1="7.874" y1="-16.256" x2="16.764" y2="-16.256" width="0.127" layer="21"/>
<wire x1="16.764" y1="-16.256" x2="20.574" y2="-16.256" width="0.127" layer="21"/>
<wire x1="20.574" y1="-16.256" x2="24.13" y2="-16.256" width="0.127" layer="21"/>
<wire x1="24.13" y1="-16.256" x2="24.765" y2="-16.256" width="0.127" layer="21"/>
<wire x1="24.765" y1="-16.256" x2="24.765" y2="3.175" width="0.127" layer="21"/>
<wire x1="24.765" y1="3.175" x2="24.765" y2="4.445" width="0.127" layer="21"/>
<wire x1="24.765" y1="4.445" x2="24.765" y2="5.715" width="0.127" layer="21"/>
<wire x1="24.765" y1="5.715" x2="24.765" y2="6.985" width="0.127" layer="21"/>
<wire x1="24.765" y1="6.985" x2="24.765" y2="8.255" width="0.127" layer="21"/>
<wire x1="24.765" y1="8.255" x2="24.765" y2="9.525" width="0.127" layer="21"/>
<wire x1="24.765" y1="9.525" x2="24.765" y2="10.795" width="0.127" layer="21"/>
<wire x1="24.765" y1="10.795" x2="24.765" y2="12.065" width="0.127" layer="21"/>
<wire x1="24.765" y1="12.065" x2="24.765" y2="13.335" width="0.127" layer="21"/>
<wire x1="24.765" y1="13.335" x2="24.765" y2="14.605" width="0.127" layer="21"/>
<wire x1="24.765" y1="14.605" x2="24.765" y2="16.256" width="0.127" layer="21"/>
<wire x1="24.765" y1="16.256" x2="19.685" y2="16.256" width="0.127" layer="21"/>
<wire x1="19.685" y1="16.256" x2="19.685" y2="14.605" width="0.127" layer="21"/>
<wire x1="19.685" y1="14.605" x2="19.685" y2="13.335" width="0.127" layer="21"/>
<wire x1="19.685" y1="13.335" x2="19.685" y2="12.065" width="0.127" layer="21"/>
<wire x1="19.685" y1="12.065" x2="19.685" y2="10.795" width="0.127" layer="21"/>
<wire x1="19.685" y1="10.795" x2="19.685" y2="9.525" width="0.127" layer="21"/>
<wire x1="19.685" y1="9.525" x2="19.685" y2="8.255" width="0.127" layer="21"/>
<wire x1="19.685" y1="8.255" x2="19.685" y2="6.985" width="0.127" layer="21"/>
<wire x1="19.685" y1="6.985" x2="19.685" y2="5.715" width="0.127" layer="21"/>
<wire x1="19.685" y1="5.715" x2="19.685" y2="4.445" width="0.127" layer="21"/>
<wire x1="19.685" y1="4.445" x2="19.685" y2="3.175" width="0.127" layer="21"/>
<wire x1="19.685" y1="3.175" x2="19.685" y2="2.54" width="0.127" layer="21"/>
<wire x1="19.685" y1="2.54" x2="-19.685" y2="2.54" width="0.127" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-19.685" y2="3.175" width="0.127" layer="21"/>
<wire x1="-19.685" y1="3.175" x2="-19.685" y2="4.445" width="0.127" layer="21"/>
<wire x1="-19.685" y1="4.445" x2="-19.685" y2="5.715" width="0.127" layer="21"/>
<wire x1="-19.685" y1="5.715" x2="-19.685" y2="6.985" width="0.127" layer="21"/>
<wire x1="-19.685" y1="6.985" x2="-19.685" y2="8.255" width="0.127" layer="21"/>
<wire x1="-19.685" y1="8.255" x2="-19.685" y2="9.525" width="0.127" layer="21"/>
<wire x1="-19.685" y1="9.525" x2="-19.685" y2="10.795" width="0.127" layer="21"/>
<wire x1="-19.685" y1="10.795" x2="-19.685" y2="12.065" width="0.127" layer="21"/>
<wire x1="-19.685" y1="12.065" x2="-19.685" y2="13.335" width="0.127" layer="21"/>
<wire x1="-19.685" y1="13.335" x2="-19.685" y2="16.256" width="0.127" layer="21"/>
<wire x1="-19.685" y1="16.256" x2="-24.765" y2="16.256" width="0.127" layer="21"/>
<pad name="A.P.1" x="-23.495" y="13.97" drill="1" shape="square"/>
<pad name="A.M.1" x="-20.955" y="13.97" drill="1" shape="square"/>
<pad name="A.P.2" x="-23.495" y="11.43" drill="1"/>
<pad name="A.M.2" x="-20.955" y="11.43" drill="1"/>
<pad name="A.P.3" x="-23.495" y="8.89" drill="1"/>
<pad name="A.M.3" x="-20.955" y="8.89" drill="1"/>
<pad name="A.M.4" x="-20.955" y="6.35" drill="1"/>
<pad name="A.P.4" x="-23.495" y="6.35" drill="1"/>
<pad name="A.P.5" x="-23.495" y="3.81" drill="1"/>
<pad name="A.M.5" x="-20.955" y="3.81" drill="1"/>
<pad name="B.P.1" x="20.955" y="13.97" drill="1" shape="square"/>
<pad name="B.M.1" x="23.495" y="13.97" drill="1" shape="square"/>
<pad name="B.P.2" x="20.955" y="11.43" drill="1"/>
<pad name="B.M.2" x="23.495" y="11.43" drill="1"/>
<pad name="B.P.3" x="20.955" y="8.89" drill="1"/>
<pad name="B.M.3" x="23.495" y="8.89" drill="1"/>
<pad name="B.M.4" x="23.495" y="6.35" drill="1"/>
<pad name="B.P.4" x="20.955" y="6.35" drill="1"/>
<pad name="B.P.5" x="20.955" y="3.81" drill="1"/>
<pad name="B.M.5" x="23.495" y="3.81" drill="1"/>
<wire x1="-13.97" y1="-16.256" x2="-13.97" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-7.62" x2="-6.35" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-7.62" x2="-6.35" y2="-16.256" width="0.127" layer="21"/>
<wire x1="7.874" y1="-16.256" x2="7.874" y2="-2.286" width="0.127" layer="21"/>
<wire x1="7.874" y1="-2.286" x2="16.764" y2="-2.286" width="0.127" layer="21"/>
<wire x1="16.764" y1="-2.286" x2="16.764" y2="-16.256" width="0.127" layer="21"/>
<wire x1="24.13" y1="-7.874" x2="20.574" y2="-7.874" width="0.127" layer="21"/>
<wire x1="20.574" y1="-7.874" x2="20.574" y2="-16.256" width="0.127" layer="21"/>
<wire x1="-20.574" y1="-7.874" x2="-24.13" y2="-7.874" width="0.127" layer="21"/>
<wire x1="-20.574" y1="-16.256" x2="-20.574" y2="-7.874" width="0.127" layer="21"/>
<wire x1="-24.13" y1="-16.256" x2="-24.13" y2="-7.874" width="0.127" layer="21"/>
<wire x1="24.13" y1="-16.256" x2="24.13" y2="-7.874" width="0.127" layer="21"/>
<text x="-22.352" y="-12.065" size="1.27" layer="21" rot="R90" align="center">SW1</text>
<text x="-10.16" y="-12.065" size="1.27" layer="21" align="center">USB</text>
<text x="12.192" y="-12.065" size="1.27" layer="21" align="center">DC-IN</text>
<text x="22.352" y="-12.065" size="1.27" layer="21" rot="R90" align="center">SW2</text>
<wire x1="-22.225" y1="14.605" x2="-21.59" y2="15.24" width="0.127" layer="21"/>
<wire x1="-21.59" y1="15.24" x2="-20.32" y2="15.24" width="0.127" layer="21"/>
<wire x1="-20.32" y1="15.24" x2="-19.685" y2="14.605" width="0.127" layer="21"/>
<wire x1="-22.225" y1="14.605" x2="-22.86" y2="15.24" width="0.127" layer="21"/>
<wire x1="-22.86" y1="15.24" x2="-24.13" y2="15.24" width="0.127" layer="21"/>
<wire x1="-24.13" y1="15.24" x2="-24.765" y2="14.605" width="0.127" layer="21"/>
<wire x1="-24.765" y1="13.335" x2="-24.13" y2="12.7" width="0.127" layer="21"/>
<wire x1="-24.13" y1="12.7" x2="-24.765" y2="12.065" width="0.127" layer="21"/>
<wire x1="-24.765" y1="10.795" x2="-24.13" y2="10.16" width="0.127" layer="21"/>
<wire x1="-24.13" y1="10.16" x2="-24.765" y2="9.525" width="0.127" layer="21"/>
<wire x1="-24.765" y1="8.255" x2="-24.13" y2="7.62" width="0.127" layer="21"/>
<wire x1="-24.13" y1="7.62" x2="-24.765" y2="6.985" width="0.127" layer="21"/>
<wire x1="-24.765" y1="5.715" x2="-24.13" y2="5.08" width="0.127" layer="21"/>
<wire x1="-24.13" y1="5.08" x2="-24.765" y2="4.445" width="0.127" layer="21"/>
<wire x1="-24.765" y1="3.175" x2="-24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="-24.13" y1="2.54" x2="-22.86" y2="2.54" width="0.127" layer="21"/>
<wire x1="-22.86" y1="2.54" x2="-22.225" y2="3.175" width="0.127" layer="21"/>
<wire x1="-22.225" y1="3.175" x2="-21.59" y2="2.54" width="0.127" layer="21"/>
<wire x1="-21.59" y1="2.54" x2="-20.32" y2="2.54" width="0.127" layer="21"/>
<wire x1="-20.32" y1="2.54" x2="-19.685" y2="3.175" width="0.127" layer="21"/>
<wire x1="-19.685" y1="4.445" x2="-20.32" y2="5.08" width="0.127" layer="21"/>
<wire x1="-20.32" y1="5.08" x2="-19.685" y2="5.715" width="0.127" layer="21"/>
<wire x1="-19.685" y1="6.985" x2="-20.32" y2="7.62" width="0.127" layer="21"/>
<wire x1="-20.32" y1="7.62" x2="-19.685" y2="8.255" width="0.127" layer="21"/>
<wire x1="-19.685" y1="9.525" x2="-20.32" y2="10.16" width="0.127" layer="21"/>
<wire x1="-20.32" y1="10.16" x2="-19.685" y2="10.795" width="0.127" layer="21"/>
<wire x1="-19.685" y1="12.065" x2="-20.32" y2="12.7" width="0.127" layer="21"/>
<wire x1="-20.32" y1="12.7" x2="-19.685" y2="13.335" width="0.127" layer="21"/>
<wire x1="19.685" y1="14.605" x2="20.32" y2="15.24" width="0.127" layer="21"/>
<wire x1="20.32" y1="15.24" x2="21.59" y2="15.24" width="0.127" layer="21"/>
<wire x1="21.59" y1="15.24" x2="22.225" y2="14.605" width="0.127" layer="21"/>
<wire x1="22.225" y1="14.605" x2="22.86" y2="15.24" width="0.127" layer="21"/>
<wire x1="22.86" y1="15.24" x2="24.13" y2="15.24" width="0.127" layer="21"/>
<wire x1="24.13" y1="15.24" x2="24.765" y2="14.605" width="0.127" layer="21"/>
<wire x1="19.685" y1="13.335" x2="20.32" y2="12.7" width="0.127" layer="21"/>
<wire x1="20.32" y1="12.7" x2="19.685" y2="12.065" width="0.127" layer="21"/>
<wire x1="19.685" y1="10.795" x2="20.32" y2="10.16" width="0.127" layer="21"/>
<wire x1="20.32" y1="10.16" x2="19.685" y2="9.525" width="0.127" layer="21"/>
<wire x1="19.685" y1="8.255" x2="20.32" y2="7.62" width="0.127" layer="21"/>
<wire x1="20.32" y1="7.62" x2="19.685" y2="6.985" width="0.127" layer="21"/>
<wire x1="19.685" y1="5.715" x2="20.32" y2="5.08" width="0.127" layer="21"/>
<wire x1="20.32" y1="5.08" x2="19.685" y2="4.445" width="0.127" layer="21"/>
<wire x1="19.685" y1="3.175" x2="20.32" y2="2.54" width="0.127" layer="21"/>
<wire x1="20.32" y1="2.54" x2="21.59" y2="2.54" width="0.127" layer="21"/>
<wire x1="21.59" y1="2.54" x2="22.225" y2="3.175" width="0.127" layer="21"/>
<wire x1="22.225" y1="3.175" x2="22.86" y2="2.54" width="0.127" layer="21"/>
<wire x1="22.86" y1="2.54" x2="24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="24.13" y1="2.54" x2="24.765" y2="3.175" width="0.127" layer="21"/>
<wire x1="24.765" y1="4.445" x2="24.13" y2="5.08" width="0.127" layer="21"/>
<wire x1="24.13" y1="5.08" x2="24.765" y2="5.715" width="0.127" layer="21"/>
<wire x1="24.765" y1="6.985" x2="24.13" y2="7.62" width="0.127" layer="21"/>
<wire x1="24.13" y1="7.62" x2="24.765" y2="8.255" width="0.127" layer="21"/>
<wire x1="24.765" y1="9.525" x2="24.13" y2="10.16" width="0.127" layer="21"/>
<wire x1="24.13" y1="10.16" x2="24.765" y2="10.795" width="0.127" layer="21"/>
<wire x1="24.765" y1="12.065" x2="24.13" y2="12.7" width="0.127" layer="21"/>
<wire x1="24.13" y1="12.7" x2="24.765" y2="13.335" width="0.127" layer="21"/>
<wire x1="-24.13" y1="1.27" x2="-22.86" y2="1.27" width="0.127" layer="21"/>
<wire x1="-23.495" y1="1.905" x2="-23.495" y2="0.635" width="0.127" layer="21"/>
<wire x1="-21.59" y1="1.27" x2="-20.32" y2="1.27" width="0.127" layer="21"/>
<text x="-22.225" y="0" size="1.27" layer="21" align="center">A</text>
<wire x1="20.32" y1="1.27" x2="21.59" y2="1.27" width="0.127" layer="21"/>
<wire x1="20.955" y1="1.905" x2="20.955" y2="0.635" width="0.127" layer="21"/>
<wire x1="22.86" y1="1.27" x2="24.13" y2="1.27" width="0.127" layer="21"/>
<text x="22.225" y="0" size="1.27" layer="21" align="center">B</text>
<text x="0" y="3.81" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-17.145" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ARDUINO-NANO-3.0">
<description>&lt;b&gt;Arduino Nano 3.0&lt;/b&gt;</description>
<pin name="D1/TX" x="-20.32" y="17.78" length="middle"/>
<pin name="D0/RX" x="-20.32" y="15.24" length="middle"/>
<pin name="RST.1" x="-20.32" y="12.7" length="middle"/>
<pin name="GND.1" x="-20.32" y="10.16" length="middle" direction="pwr"/>
<pin name="D2" x="-20.32" y="7.62" length="middle"/>
<pin name="D3" x="-20.32" y="5.08" length="middle"/>
<pin name="D4" x="-20.32" y="2.54" length="middle"/>
<pin name="D5" x="-20.32" y="0" length="middle"/>
<pin name="D6" x="-20.32" y="-2.54" length="middle"/>
<pin name="D7" x="-20.32" y="-5.08" length="middle"/>
<pin name="D8" x="-20.32" y="-7.62" length="middle"/>
<pin name="D9" x="-20.32" y="-10.16" length="middle"/>
<pin name="D10" x="-20.32" y="-12.7" length="middle"/>
<pin name="D11/MOSI" x="-20.32" y="-15.24" length="middle"/>
<pin name="D12/MISO" x="-20.32" y="-17.78" length="middle"/>
<pin name="VIN" x="17.78" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="GND.2" x="17.78" y="15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="RST.2" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="5V" x="17.78" y="10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="A7" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="A6" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="A5" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="A4" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="A3" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="A2" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="A1" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="A0" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="AREF" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="3V3" x="17.78" y="-15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="D13/SCK" x="17.78" y="-17.78" length="middle" rot="R180"/>
<wire x1="-15.24" y1="27.94" x2="12.7" y2="27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="27.94" x2="12.7" y2="-30.48" width="0.254" layer="94"/>
<wire x1="12.7" y1="-30.48" x2="5.08" y2="-30.48" width="0.254" layer="94"/>
<wire x1="5.08" y1="-30.48" x2="-7.62" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-30.48" x2="-15.24" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-30.48" x2="-15.24" y2="27.94" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-30.48" x2="-7.62" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-20.32" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="-20.32" x2="5.08" y2="-30.48" width="0.254" layer="94"/>
<text x="-1.27" y="-25.4" size="2.54" layer="94" align="center">USB</text>
<text x="15.24" y="25.4" size="1.778" layer="95">&gt;NAME</text>
<text x="15.24" y="22.86" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="GAS-SENSOR-MQ2">
<description>&lt;b&gt;Gas Sensor&lt;/b&gt; based on &lt;b&gt;MQ-2&lt;/b&gt; device</description>
<pin name="VCC" x="-20.32" y="2.54" length="middle" direction="pwr"/>
<pin name="GND" x="-20.32" y="0" length="middle" direction="pwr"/>
<pin name="DO" x="-20.32" y="-2.54" length="middle" direction="out"/>
<wire x1="-15.24" y1="7.62" x2="-15.24" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-10.16" x2="15.24" y2="-10.16" width="0.254" layer="94"/>
<wire x1="15.24" y1="-10.16" x2="15.24" y2="7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="7.62" x2="-15.24" y2="7.62" width="0.254" layer="94"/>
<circle x="5.08" y="-1.27" radius="7.62" width="0.254" layer="94"/>
<text x="-15.24" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-15.24" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="AO" x="-20.32" y="-5.08" length="middle" direction="out"/>
</symbol>
<symbol name="LIGHT-SENSOR-BH1750">
<description>&lt;b&gt;Digital Light Sensor&lt;/b&gt; based on &lt;b&gt;BH1750&lt;/b&gt;</description>
<pin name="ADDR" x="-17.78" y="5.08" length="middle"/>
<pin name="GND" x="-17.78" y="-2.54" length="middle" direction="pwr"/>
<pin name="SCL" x="-17.78" y="0" length="middle"/>
<pin name="SDA" x="-17.78" y="2.54" length="middle"/>
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<text x="-12.7" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="12.7" size="1.778" layer="95">&gt;VALUE</text>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="3.302" y1="-2.286" x2="3.302" y2="2.286" width="0.254" layer="94"/>
<wire x1="3.302" y1="2.286" x2="1.016" y2="2.286" width="0.254" layer="94"/>
<wire x1="1.016" y1="2.286" x2="1.016" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.016" y1="-2.286" x2="3.302" y2="-2.286" width="0.254" layer="94"/>
<pin name="VCC" x="-17.78" y="-5.08" length="middle" direction="pwr"/>
<circle x="7.62" y="5.08" radius="2.54" width="0.254" layer="94"/>
<circle x="7.62" y="-5.08" radius="2.54" width="0.254" layer="94"/>
<text x="0" y="0" size="2.032" layer="94" rot="R270" align="top-center">BH1750</text>
</symbol>
<symbol name="SOUND-DETECTOR">
<description>&lt;b&gt;Sound Detection Module&lt;/b&gt;</description>
<pin name="GND" x="-20.32" y="2.54" length="middle" direction="pwr"/>
<pin name="OUT" x="-20.32" y="0" length="middle" direction="out"/>
<pin name="+5V" x="-20.32" y="-2.54" length="middle" direction="pwr"/>
<wire x1="-15.24" y1="7.62" x2="-15.24" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-7.62" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="7.62" x2="-15.24" y2="7.62" width="0.254" layer="94"/>
<circle x="5.08" y="0" radius="5.08" width="0.254" layer="94"/>
<text x="5.08" y="0" size="2.54" layer="94" align="center">MIC</text>
<text x="-15.24" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-15.24" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TEMP-HUM-PRES-BME280">
<description>&lt;b&gt;Digital Humidity-Temperature-Pressure Sensor&lt;/b&gt; based on &lt;b&gt;BME280&lt;/b&gt; device</description>
<pin name="VIN" x="-15.24" y="5.08" length="middle" direction="pwr"/>
<pin name="GND" x="-15.24" y="2.54" length="middle" direction="pwr"/>
<pin name="SCL" x="-15.24" y="0" length="middle"/>
<pin name="SDA" x="-15.24" y="-2.54" length="middle"/>
<wire x1="-10.16" y1="10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="12.7" size="1.778" layer="95">&gt;VALUE</text>
<wire x1="7.62" y1="10.16" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<circle x="0.762" y="-2.54" radius="0.359209375" width="0.254" layer="94"/>
</symbol>
<symbol name="WIRELESS-NRF24L01">
<description>&lt;b&gt;2.4 GHz Wireless Module&lt;/b&gt; based on &lt;b&gt;NRF24L01&lt;/b&gt; chip</description>
<pin name="GND" x="-22.86" y="10.16" length="middle" direction="pwr"/>
<pin name="VCC" x="-22.86" y="7.62" length="middle" direction="pwr"/>
<pin name="CE" x="-22.86" y="5.08" length="middle"/>
<pin name="CSN" x="-22.86" y="2.54" length="middle"/>
<pin name="SCK" x="-22.86" y="0" length="middle"/>
<pin name="MOSI" x="-22.86" y="-2.54" length="middle"/>
<pin name="MISO" x="-22.86" y="-5.08" length="middle"/>
<pin name="IRQ" x="-22.86" y="-7.62" length="middle"/>
<wire x1="-17.78" y1="12.7" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="17.78" y2="12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="12.7" x2="17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="-17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-10.16" x2="-17.78" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<text x="12.7" y="1.27" size="2.54" layer="94" rot="R90" align="center">ANTENNA</text>
<text x="20.32" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="20.32" y="7.62" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="BREADBOARD-POWER-V1">
<description>&lt;b&gt;Breadboard Power Adapter&lt;/b&gt; with onboard 3.3V and 5V regulators</description>
<pin name="P.1" x="-10.16" y="5.08" visible="off" length="middle"/>
<pin name="P.2" x="-10.16" y="2.54" visible="off" length="middle"/>
<pin name="P.3" x="-10.16" y="0" visible="off" length="middle"/>
<pin name="P.4" x="-10.16" y="-2.54" visible="off" length="middle"/>
<pin name="P.5" x="-10.16" y="-5.08" visible="off" length="middle"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<pin name="M.1" x="10.16" y="5.08" visible="off" length="middle" rot="R180"/>
<pin name="M.2" x="10.16" y="2.54" visible="off" length="middle" rot="R180"/>
<pin name="M.3" x="10.16" y="0" visible="off" length="middle" rot="R180"/>
<pin name="M.4" x="10.16" y="-2.54" visible="off" length="middle" rot="R180"/>
<pin name="M.5" x="10.16" y="-5.08" visible="off" length="middle" rot="R180"/>
<wire x1="-2.54" y1="5.08" x2="-4.064" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.302" y1="4.318" x2="-3.302" y2="5.842" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-4.064" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.302" y1="1.778" x2="-3.302" y2="3.302" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-4.064" y2="0" width="0.254" layer="94"/>
<wire x1="-3.302" y1="-0.762" x2="-3.302" y2="0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-4.064" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.302" y1="-3.302" x2="-3.302" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-4.064" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-3.302" y1="-5.842" x2="-3.302" y2="-4.318" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="4.064" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="4.064" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="4.064" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="4.064" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="4.064" y2="-5.08" width="0.254" layer="94"/>
<text x="-5.08" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ARDUINO-NANO-3.0" prefix="M">
<description>&lt;b&gt;Arduino Nano 3.0&lt;/b&gt; and compatible devices
&lt;p&gt;More details available here:&lt;br&gt;
&lt;a href="https://www.arduino.cc/en/Main/ArduinoBoardNano"&gt;https://www.arduino.cc/en/Main/ArduinoBoardNano&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/arduino+nano"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;/p&gt;

&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=ARDUINO-NANO-3.0"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="ARDUINO-NANO-3.0" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ARDUINO-NANO-3.0">
<connects>
<connect gate="G$1" pin="3V3" pad="J2.14"/>
<connect gate="G$1" pin="5V" pad="J2.4"/>
<connect gate="G$1" pin="A0" pad="J2.12"/>
<connect gate="G$1" pin="A1" pad="J2.11"/>
<connect gate="G$1" pin="A2" pad="J2.10"/>
<connect gate="G$1" pin="A3" pad="J2.9"/>
<connect gate="G$1" pin="A4" pad="J2.8"/>
<connect gate="G$1" pin="A5" pad="J2.7"/>
<connect gate="G$1" pin="A6" pad="J2.6"/>
<connect gate="G$1" pin="A7" pad="J2.5"/>
<connect gate="G$1" pin="AREF" pad="J2.13"/>
<connect gate="G$1" pin="D0/RX" pad="J1.2"/>
<connect gate="G$1" pin="D1/TX" pad="J1.1"/>
<connect gate="G$1" pin="D10" pad="J1.13"/>
<connect gate="G$1" pin="D11/MOSI" pad="J1.14"/>
<connect gate="G$1" pin="D12/MISO" pad="J1.15"/>
<connect gate="G$1" pin="D13/SCK" pad="J2.15"/>
<connect gate="G$1" pin="D2" pad="J1.5"/>
<connect gate="G$1" pin="D3" pad="J1.6"/>
<connect gate="G$1" pin="D4" pad="J1.7"/>
<connect gate="G$1" pin="D5" pad="J1.8"/>
<connect gate="G$1" pin="D6" pad="J1.9"/>
<connect gate="G$1" pin="D7" pad="J1.10"/>
<connect gate="G$1" pin="D8" pad="J1.11"/>
<connect gate="G$1" pin="D9" pad="J1.12"/>
<connect gate="G$1" pin="GND.1" pad="J1.4"/>
<connect gate="G$1" pin="GND.2" pad="J2.2"/>
<connect gate="G$1" pin="RST.1" pad="J1.3"/>
<connect gate="G$1" pin="RST.2" pad="J2.3"/>
<connect gate="G$1" pin="VIN" pad="J2.1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GAS-SENSOR-MQ2">
<description>&lt;b&gt;Gas Sensor&lt;/b&gt; based on &lt;b&gt;MQ-2&lt;/b&gt; device
&lt;p&gt;Guide describing how to use &lt;b&gt;MQ-2&lt;/b&gt; sensor with Arduino is available here:&lt;br /&gt;
&lt;a href="http://www.instructables.com/id/How-to-use-MQ2-Gas-Sensor-Arduino-Tutorial/"&gt;http://www.instructables.com/id/How-to-use-MQ2-Gas-Sensor-Arduino-Tutorial/&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;MQ-2&lt;/b&gt; sensor datasheet is available here:&lt;br /&gt;
&lt;a href="http://gas-sensor.ru/pdf/combustible-gas-sensor.pdf"&gt;http://gas-sensor.ru/pdf/combustible-gas-sensor.pdf&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/mq2+sensor+module+smoke"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=GAS-SENSOR-MQ2"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="GAS-SENSOR-MQ2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GAS-SENSOR-MQ2">
<connects>
<connect gate="G$1" pin="AO" pad="4"/>
<connect gate="G$1" pin="DO" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LIGHT-SENSOR-BH1750">
<description>&lt;b&gt;Digital Light Sensor&lt;/b&gt; based on &lt;b&gt;BH1750&lt;/b&gt;
&lt;p&gt;&lt;b&gt;BH1750&lt;/b&gt; datasheet is available here:&lt;br /&gt;
&lt;a href="http://www.mouser.com/ds/2/348/bh1750fvi-e-186247.pdf"&gt;http://www.mouser.com/ds/2/348/bh1750fvi-e-186247.pdf&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Arduino Library&lt;/b&gt; for &lt;b&gt;BH1750&lt;/b&gt; is available here:&lt;br /&gt;
&lt;a href="https://github.com/claws/BH1750"&gt;https://github.com/claws/BH1750&lt;/a&gt;&lt;p/&gt;
&lt;p&gt;&lt;b&gt;Video Tutorial&lt;/b&gt; is available here:&lt;br /&gt;
&lt;a href="https://www.youtube.com/watch?v=UsLcHSbxf_8"&gt;https://www.youtube.com/watch?v=UsLcHSbxf_8&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/bh1750fvi+module"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;br /&gt;
&lt;b&gt;Note:&lt;/b&gt; There are many variants. Search for the proper version.&lt;/p&gt;
&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=LIGHT-SENSOR-BH1750"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="LIGHT-SENSOR-BH1750" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LIGHT-SENSOR-BH1750">
<connects>
<connect gate="G$1" pin="ADDR" pad="5"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="SCL" pad="3"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOUND-DETECTOR">
<description>&lt;b&gt;Sound Detection Module&lt;/b&gt;
&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=SOUND-DETECTOR"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SOUND-DETECTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOUND-DETECTOR">
<connects>
<connect gate="G$1" pin="+5V" pad="3"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="OUT" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TEMP-HUM-PRES-BME280">
<description>&lt;b&gt;Digital Humidity-Temperature-Pressure Sensor&lt;/b&gt; based on &lt;b&gt;BME280&lt;/b&gt; device
&lt;p&gt;More information about &lt;b&gt;BME280&lt;/b&gt; device is available here:&lt;br&gt;
&lt;a href="https://www.bosch-sensortec.com/bst/products/all_products/bme280"&gt;https://www.bosch-sensortec.com/bst/products/all_products/bme280&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/bme280+breakout"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=TEMP-HUM-PRES-BME280"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="TEMP-HUM-PRES-BME280" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TEMP-HUM-PRES-BME280">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="SCL" pad="3"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="VIN" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WIRELESS-NRF24L01" prefix="M">
<description>&lt;b&gt;2.4 GHz Wireless Module&lt;/b&gt; based on &lt;b&gt;NRF24L01&lt;/b&gt; chip
&lt;p&gt;More details available here:&lt;br&gt;
&lt;a href="http://www.electrodragon.com/w/NRF24L01"&gt;http://www.electrodragon.com/w/NRF24L01&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/nrf24l01+arduino"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;/p&gt;

&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=WIRELESS-NRF24L01"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="WIRELESS-NRF24L01" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WIRELESS-NRF24L01">
<connects>
<connect gate="G$1" pin="CE" pad="3"/>
<connect gate="G$1" pin="CSN" pad="4"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IRQ" pad="8"/>
<connect gate="G$1" pin="MISO" pad="7"/>
<connect gate="G$1" pin="MOSI" pad="6"/>
<connect gate="G$1" pin="SCK" pad="5"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BREADBOARD-POWER-V1" uservalue="yes">
<description>&lt;b&gt;Breadboard Power Adapter&lt;/b&gt; with onboard 3.3V and 5V regulators
&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=BREADBOARD-POWER-V1"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="BREADBOARD-POWER-V1" x="-17.78" y="0"/>
<gate name="G$2" symbol="BREADBOARD-POWER-V1" x="15.24" y="0"/>
</gates>
<devices>
<device name="" package="BREADBOARD-POWER-V1">
<connects>
<connect gate="G$1" pin="M.1" pad="A.M.1"/>
<connect gate="G$1" pin="M.2" pad="A.M.2"/>
<connect gate="G$1" pin="M.3" pad="A.M.3"/>
<connect gate="G$1" pin="M.4" pad="A.M.4"/>
<connect gate="G$1" pin="M.5" pad="A.M.5"/>
<connect gate="G$1" pin="P.1" pad="A.P.1"/>
<connect gate="G$1" pin="P.2" pad="A.P.2"/>
<connect gate="G$1" pin="P.3" pad="A.P.3"/>
<connect gate="G$1" pin="P.4" pad="A.P.4"/>
<connect gate="G$1" pin="P.5" pad="A.P.5"/>
<connect gate="G$2" pin="M.1" pad="B.M.1"/>
<connect gate="G$2" pin="M.2" pad="B.M.2"/>
<connect gate="G$2" pin="M.3" pad="B.M.3"/>
<connect gate="G$2" pin="M.4" pad="B.M.4"/>
<connect gate="G$2" pin="M.5" pad="B.M.5"/>
<connect gate="G$2" pin="P.1" pad="B.P.1"/>
<connect gate="G$2" pin="P.2" pad="B.P.2"/>
<connect gate="G$2" pin="P.3" pad="B.P.3"/>
<connect gate="G$2" pin="P.4" pad="B.P.4"/>
<connect gate="G$2" pin="P.5" pad="B.P.5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="+" library="diy-modules" deviceset="ARDUINO-NANO-3.0" device=""/>
<part name="S1" library="diy-modules" deviceset="GAS-SENSOR-MQ2" device=""/>
<part name="S2" library="diy-modules" deviceset="LIGHT-SENSOR-BH1750" device=""/>
<part name="S4" library="diy-modules" deviceset="SOUND-DETECTOR" device=""/>
<part name="S3" library="diy-modules" deviceset="TEMP-HUM-PRES-BME280" device=""/>
<part name="SN1" library="diy-modules" deviceset="WIRELESS-NRF24L01" device=""/>
<part name="5V****" library="diy-modules" deviceset="BREADBOARD-POWER-V1" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="+" gate="G$1" x="93.98" y="58.42" smashed="yes">
<attribute name="NAME" x="109.22" y="83.82" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.22" y="81.28" size="1.778" layer="96"/>
</instance>
<instance part="S1" gate="G$1" x="213.36" y="7.62" smashed="yes">
<attribute name="NAME" x="198.12" y="20.32" size="1.778" layer="95"/>
<attribute name="VALUE" x="198.12" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="S2" gate="G$1" x="210.82" y="55.88" smashed="yes">
<attribute name="NAME" x="198.12" y="71.12" size="1.778" layer="95"/>
<attribute name="VALUE" x="198.12" y="68.58" size="1.778" layer="95"/>
</instance>
<instance part="S4" gate="G$1" x="213.36" y="30.48" smashed="yes">
<attribute name="NAME" x="198.12" y="43.18" size="1.778" layer="95"/>
<attribute name="VALUE" x="198.12" y="40.64" size="1.778" layer="96"/>
</instance>
<instance part="S3" gate="G$1" x="208.28" y="81.28" smashed="yes">
<attribute name="NAME" x="198.12" y="96.52" size="1.778" layer="95"/>
<attribute name="VALUE" x="198.12" y="93.98" size="1.778" layer="95"/>
</instance>
<instance part="SN1" gate="G$1" x="17.78" y="35.56" smashed="yes" rot="MR0">
<attribute name="NAME" x="-2.54" y="45.72" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-2.54" y="43.18" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="5V****" gate="G$1" x="172.72" y="116.84" smashed="yes">
<attribute name="NAME" x="167.64" y="129.54" size="1.778" layer="95"/>
<attribute name="VALUE" x="167.64" y="127" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="VCC"/>
<pinref part="5V****" gate="G$1" pin="P.5"/>
<wire x1="193.04" y1="50.8" x2="162.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="162.56" y1="50.8" x2="162.56" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="GND"/>
<pinref part="5V****" gate="G$1" pin="M.5"/>
<wire x1="193.04" y1="53.34" x2="182.88" y2="53.34" width="0.1524" layer="91"/>
<wire x1="182.88" y1="53.34" x2="182.88" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="VIN"/>
<wire x1="193.04" y1="86.36" x2="160.02" y2="86.36" width="0.1524" layer="91"/>
<wire x1="160.02" y1="86.36" x2="160.02" y2="114.3" width="0.1524" layer="91"/>
<pinref part="5V****" gate="G$1" pin="P.4"/>
<wire x1="160.02" y1="114.3" x2="162.56" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="GND"/>
<wire x1="193.04" y1="83.82" x2="185.42" y2="83.82" width="0.1524" layer="91"/>
<wire x1="185.42" y1="83.82" x2="185.42" y2="114.3" width="0.1524" layer="91"/>
<pinref part="5V****" gate="G$1" pin="M.4"/>
<wire x1="185.42" y1="114.3" x2="182.88" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D1/TX" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D1/TX"/>
<wire x1="73.66" y1="76.2" x2="53.34" y2="76.2" width="0.1524" layer="91"/>
<label x="55.88" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="D0/RX" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D0/RX"/>
<wire x1="73.66" y1="73.66" x2="53.34" y2="73.66" width="0.1524" layer="91"/>
<label x="55.88" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="RST.1" class="0">
<segment>
<pinref part="+" gate="G$1" pin="RST.1"/>
<wire x1="73.66" y1="71.12" x2="53.34" y2="71.12" width="0.1524" layer="91"/>
<label x="55.88" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND.1" class="0">
<segment>
<pinref part="+" gate="G$1" pin="GND.1"/>
<wire x1="73.66" y1="68.58" x2="40.64" y2="68.58" width="0.1524" layer="91"/>
<label x="55.88" y="68.58" size="1.778" layer="95"/>
<pinref part="SN1" gate="G$1" pin="GND"/>
<wire x1="40.64" y1="45.72" x2="40.64" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D2"/>
<wire x1="73.66" y1="66.04" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
<label x="55.88" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D3"/>
<wire x1="73.66" y1="63.5" x2="53.34" y2="63.5" width="0.1524" layer="91"/>
<label x="55.88" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="D4" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D4"/>
<wire x1="73.66" y1="60.96" x2="53.34" y2="60.96" width="0.1524" layer="91"/>
<label x="55.88" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D5"/>
<wire x1="73.66" y1="58.42" x2="53.34" y2="58.42" width="0.1524" layer="91"/>
<label x="55.88" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D6"/>
<wire x1="73.66" y1="55.88" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
<label x="55.88" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="D7" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D7"/>
<wire x1="73.66" y1="53.34" x2="43.18" y2="53.34" width="0.1524" layer="91"/>
<label x="55.88" y="53.34" size="1.778" layer="95"/>
<pinref part="SN1" gate="G$1" pin="CE"/>
<wire x1="40.64" y1="40.64" x2="43.18" y2="40.64" width="0.1524" layer="91"/>
<wire x1="43.18" y1="40.64" x2="43.18" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D8" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D8"/>
<wire x1="73.66" y1="50.8" x2="48.26" y2="50.8" width="0.1524" layer="91"/>
<label x="55.88" y="50.8" size="1.778" layer="95"/>
<wire x1="48.26" y1="50.8" x2="48.26" y2="38.1" width="0.1524" layer="91"/>
<pinref part="SN1" gate="G$1" pin="CSN"/>
<wire x1="48.26" y1="38.1" x2="40.64" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D9" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D9"/>
<wire x1="73.66" y1="48.26" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
<label x="55.88" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="D10" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D10"/>
<wire x1="73.66" y1="45.72" x2="53.34" y2="45.72" width="0.1524" layer="91"/>
<label x="55.88" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="D11/MOSI" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D11/MOSI"/>
<wire x1="73.66" y1="43.18" x2="50.8" y2="43.18" width="0.1524" layer="91"/>
<label x="55.88" y="43.18" size="1.778" layer="95"/>
<wire x1="50.8" y1="43.18" x2="50.8" y2="33.02" width="0.1524" layer="91"/>
<pinref part="SN1" gate="G$1" pin="MOSI"/>
<wire x1="50.8" y1="33.02" x2="40.64" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D12/MISO" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D12/MISO"/>
<wire x1="73.66" y1="40.64" x2="53.34" y2="40.64" width="0.1524" layer="91"/>
<label x="55.88" y="40.64" size="1.778" layer="95"/>
<pinref part="SN1" gate="G$1" pin="MISO"/>
<wire x1="40.64" y1="30.48" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
<wire x1="53.34" y1="30.48" x2="53.34" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="+" gate="G$1" pin="VIN"/>
<wire x1="111.76" y1="76.2" x2="127" y2="76.2" width="0.1524" layer="91"/>
<label x="124.46" y="76.2" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="GND.2" class="0">
<segment>
<pinref part="+" gate="G$1" pin="GND.2"/>
<wire x1="111.76" y1="73.66" x2="129.54" y2="73.66" width="0.1524" layer="91"/>
<label x="124.46" y="73.66" size="1.778" layer="95" align="bottom-right"/>
<pinref part="5V****" gate="G$1" pin="M.1"/>
<wire x1="182.88" y1="121.92" x2="182.88" y2="127" width="0.1524" layer="91"/>
<wire x1="182.88" y1="127" x2="129.54" y2="127" width="0.1524" layer="91"/>
<wire x1="129.54" y1="127" x2="129.54" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RST.2" class="0">
<segment>
<pinref part="+" gate="G$1" pin="RST.2"/>
<wire x1="111.76" y1="71.12" x2="127" y2="71.12" width="0.1524" layer="91"/>
<label x="124.46" y="71.12" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="+" gate="G$1" pin="5V"/>
<wire x1="111.76" y1="68.58" x2="127" y2="68.58" width="0.1524" layer="91"/>
<label x="124.46" y="68.58" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<pinref part="+" gate="G$1" pin="A7"/>
<wire x1="111.76" y1="66.04" x2="127" y2="66.04" width="0.1524" layer="91"/>
<label x="124.46" y="66.04" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<pinref part="+" gate="G$1" pin="A6"/>
<wire x1="111.76" y1="63.5" x2="127" y2="63.5" width="0.1524" layer="91"/>
<label x="124.46" y="63.5" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<pinref part="+" gate="G$1" pin="A5"/>
<wire x1="111.76" y1="60.96" x2="134.62" y2="60.96" width="0.1524" layer="91"/>
<label x="124.46" y="60.96" size="1.778" layer="95" align="bottom-right"/>
<pinref part="S3" gate="G$1" pin="SCL"/>
<wire x1="193.04" y1="81.28" x2="134.62" y2="81.28" width="0.1524" layer="91"/>
<wire x1="134.62" y1="81.28" x2="134.62" y2="60.96" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="SCL"/>
<wire x1="134.62" y1="55.88" x2="193.04" y2="55.88" width="0.1524" layer="91"/>
<wire x1="134.62" y1="55.88" x2="134.62" y2="60.96" width="0.1524" layer="91"/>
<junction x="134.62" y="60.96"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<pinref part="+" gate="G$1" pin="A4"/>
<label x="124.46" y="58.42" size="1.778" layer="95" align="bottom-right"/>
<pinref part="S3" gate="G$1" pin="SDA"/>
<wire x1="193.04" y1="78.74" x2="139.7" y2="78.74" width="0.1524" layer="91"/>
<wire x1="139.7" y1="78.74" x2="139.7" y2="58.42" width="0.1524" layer="91"/>
<wire x1="111.76" y1="58.42" x2="139.7" y2="58.42" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="SDA"/>
<wire x1="139.7" y1="58.42" x2="193.04" y2="58.42" width="0.1524" layer="91"/>
<junction x="139.7" y="58.42"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="+" gate="G$1" pin="A3"/>
<wire x1="111.76" y1="55.88" x2="127" y2="55.88" width="0.1524" layer="91"/>
<label x="124.46" y="55.88" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="+" gate="G$1" pin="A2"/>
<wire x1="111.76" y1="53.34" x2="127" y2="53.34" width="0.1524" layer="91"/>
<label x="124.46" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="+" gate="G$1" pin="A1"/>
<wire x1="111.76" y1="50.8" x2="127" y2="50.8" width="0.1524" layer="91"/>
<label x="124.46" y="50.8" size="1.778" layer="95" align="bottom-right"/>
<pinref part="S4" gate="G$1" pin="OUT"/>
<wire x1="193.04" y1="30.48" x2="134.62" y2="30.48" width="0.1524" layer="91"/>
<wire x1="134.62" y1="30.48" x2="134.62" y2="50.8" width="0.1524" layer="91"/>
<wire x1="134.62" y1="50.8" x2="111.76" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="+" gate="G$1" pin="A0"/>
<wire x1="111.76" y1="48.26" x2="127" y2="48.26" width="0.1524" layer="91"/>
<label x="124.46" y="48.26" size="1.778" layer="95" align="bottom-right"/>
<pinref part="S1" gate="G$1" pin="AO"/>
<wire x1="193.04" y1="2.54" x2="132.08" y2="2.54" width="0.1524" layer="91"/>
<wire x1="132.08" y1="2.54" x2="132.08" y2="48.26" width="0.1524" layer="91"/>
<wire x1="132.08" y1="48.26" x2="111.76" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AREF" class="0">
<segment>
<pinref part="+" gate="G$1" pin="AREF"/>
<wire x1="111.76" y1="45.72" x2="127" y2="45.72" width="0.1524" layer="91"/>
<label x="124.46" y="45.72" size="1.778" layer="95" align="bottom-right"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="SN1" gate="G$1" pin="VCC"/>
<wire x1="40.64" y1="43.18" x2="45.72" y2="43.18" width="0.1524" layer="91"/>
<wire x1="45.72" y1="43.18" x2="45.72" y2="20.32" width="0.1524" layer="91"/>
<wire x1="45.72" y1="20.32" x2="129.54" y2="20.32" width="0.1524" layer="91"/>
<wire x1="129.54" y1="20.32" x2="129.54" y2="43.18" width="0.1524" layer="91"/>
<pinref part="+" gate="G$1" pin="3V3"/>
<wire x1="111.76" y1="43.18" x2="124.46" y2="43.18" width="0.1524" layer="91"/>
<label x="124.46" y="43.18" size="1.778" layer="95" align="bottom-right"/>
<wire x1="124.46" y1="43.18" x2="127" y2="43.18" width="0.1524" layer="91"/>
<wire x1="127" y1="43.18" x2="129.54" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D13/SCK" class="0">
<segment>
<pinref part="+" gate="G$1" pin="D13/SCK"/>
<wire x1="111.76" y1="40.64" x2="127" y2="40.64" width="0.1524" layer="91"/>
<label x="124.46" y="40.64" size="1.778" layer="95" align="bottom-right"/>
<pinref part="SN1" gate="G$1" pin="SCK"/>
<wire x1="40.64" y1="35.56" x2="43.18" y2="35.56" width="0.1524" layer="91"/>
<wire x1="43.18" y1="35.56" x2="43.18" y2="17.78" width="0.1524" layer="91"/>
<wire x1="43.18" y1="17.78" x2="139.7" y2="17.78" width="0.1524" layer="91"/>
<wire x1="139.7" y1="17.78" x2="139.7" y2="40.64" width="0.1524" layer="91"/>
<wire x1="139.7" y1="40.64" x2="111.76" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="S4" gate="G$1" pin="GND"/>
<wire x1="193.04" y1="33.02" x2="187.96" y2="33.02" width="0.1524" layer="91"/>
<wire x1="187.96" y1="33.02" x2="187.96" y2="116.84" width="0.1524" layer="91"/>
<pinref part="5V****" gate="G$1" pin="M.3"/>
<wire x1="187.96" y1="116.84" x2="182.88" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="S4" gate="G$1" pin="+5V"/>
<wire x1="193.04" y1="27.94" x2="157.48" y2="27.94" width="0.1524" layer="91"/>
<wire x1="157.48" y1="27.94" x2="157.48" y2="116.84" width="0.1524" layer="91"/>
<pinref part="5V****" gate="G$1" pin="P.3"/>
<wire x1="157.48" y1="116.84" x2="162.56" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="GND"/>
<wire x1="193.04" y1="7.62" x2="154.94" y2="7.62" width="0.1524" layer="91"/>
<wire x1="154.94" y1="7.62" x2="154.94" y2="119.38" width="0.1524" layer="91"/>
<pinref part="5V****" gate="G$1" pin="P.2"/>
<wire x1="154.94" y1="119.38" x2="162.56" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="5V****" gate="G$1" pin="M.2"/>
<wire x1="190.5" y1="119.38" x2="182.88" y2="119.38" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="VCC"/>
<wire x1="193.04" y1="10.16" x2="190.5" y2="10.16" width="0.1524" layer="91"/>
<wire x1="190.5" y1="10.16" x2="190.5" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="9.0" severity="warning">
Since Version 9.0, EAGLE supports the align property for labels. 
Labels in schematic will not be understood with this version. Update EAGLE to the latest version 
for full support of labels. 
</note>
</compatibility>
</eagle>
